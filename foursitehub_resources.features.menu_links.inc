<?php
/**
 * @file
 * foursitehub_resources.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function foursitehub_resources_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu:resources
  $menu_links['main-menu:resources'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'resources',
    'router_path' => 'resources',
    'link_title' => 'Resources',
    'options' => array(),
    'module' => 'system',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-44',
  );
  // Exported menu link: navigation:node/add/foursitehub-resource
  $menu_links['navigation:node/add/foursitehub-resource'] = array(
    'menu_name' => 'navigation',
    'link_path' => 'node/add/foursitehub-resource',
    'router_path' => 'node/add/foursitehub-resource',
    'link_title' => 'Resource Item',
    'options' => array(),
    'module' => 'system',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/add',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Resource Item');
  t('Resources');


  return $menu_links;
}
