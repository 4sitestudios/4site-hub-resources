<?php
/**
 * @file
 * foursitehub_resources.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function foursitehub_resources_user_default_permissions() {
  $permissions = array();

  // Exported permission: create foursitehub_resource content.
  $permissions['create foursitehub_resource content'] = array(
    'name' => 'create foursitehub_resource content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: delete any foursitehub_resource content.
  $permissions['delete any foursitehub_resource content'] = array(
    'name' => 'delete any foursitehub_resource content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: delete own foursitehub_resource content.
  $permissions['delete own foursitehub_resource content'] = array(
    'name' => 'delete own foursitehub_resource content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: edit any foursitehub_resource content.
  $permissions['edit any foursitehub_resource content'] = array(
    'name' => 'edit any foursitehub_resource content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: edit own foursitehub_resource content.
  $permissions['edit own foursitehub_resource content'] = array(
    'name' => 'edit own foursitehub_resource content',
    'roles' => array(),
    'module' => 'node',
  );

  return $permissions;
}
