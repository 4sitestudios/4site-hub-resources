<?php
/**
 * @file
 * foursitehub_resources.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function foursitehub_resources_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => 'External Link',
    'description' => '',
    'format' => 'wysiwyg',
    'weight' => '0',
    'uuid' => '83ab349b-4fdc-4a50-9039-e1794cabbee9',
    'vocabulary_machine_name' => 'resource_category',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Article',
    'description' => '',
    'format' => 'wysiwyg',
    'weight' => '0',
    'uuid' => 'ac0349af-5df4-4e4e-b56c-53e2a66eb2cf',
    'vocabulary_machine_name' => 'resource_category',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Blog Post',
    'description' => '',
    'format' => 'wysiwyg',
    'weight' => '0',
    'uuid' => 'bc5b1647-a4b4-405b-9fa5-7ab4e1d2c355',
    'vocabulary_machine_name' => 'resource_category',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Video',
    'description' => '',
    'format' => 'wysiwyg',
    'weight' => '0',
    'uuid' => 'f874a18a-ae80-4715-97b5-c0cc3030d743',
    'vocabulary_machine_name' => 'resource_category',
    'metatags' => array(),
  );
  return $terms;
}
