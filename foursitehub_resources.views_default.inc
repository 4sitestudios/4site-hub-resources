<?php
/**
 * @file
 * foursitehub_resources.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function foursitehub_resources_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'resources';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Resources';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Resources';
  $handler->display->display_options['css_class'] = 'resource_category_article';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['style_options']['grouping'] = array(
    0 => array(
      'field' => 'field_resources_type',
      'rendered' => 1,
      'rendered_strip' => 0,
    ),
  );
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['hide_empty'] = TRUE;
  $handler->display->display_options['row_options']['default_field_elements'] = FALSE;
  /* Field: Content: Path */
  $handler->display->display_options['fields']['path']['id'] = 'path';
  $handler->display->display_options['fields']['path']['table'] = 'node';
  $handler->display->display_options['fields']['path']['field'] = 'path';
  $handler->display->display_options['fields']['path']['label'] = '';
  $handler->display->display_options['fields']['path']['exclude'] = TRUE;
  $handler->display->display_options['fields']['path']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['path']['absolute'] = TRUE;
  /* Field: Content: Link */
  $handler->display->display_options['fields']['field_link']['id'] = 'field_link';
  $handler->display->display_options['fields']['field_link']['table'] = 'field_data_field_link';
  $handler->display->display_options['fields']['field_link']['field'] = 'field_link';
  $handler->display->display_options['fields']['field_link']['label'] = '';
  $handler->display->display_options['fields']['field_link']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_link']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_link']['empty'] = '[path]';
  $handler->display->display_options['fields']['field_link']['hide_alter_empty'] = FALSE;
  $handler->display->display_options['fields']['field_link']['click_sort_column'] = 'url';
  $handler->display->display_options['fields']['field_link']['type'] = 'link_plain';
  $handler->display->display_options['fields']['field_link']['delta_offset'] = '0';
  /* Field: Content: Internal Link */
  $handler->display->display_options['fields']['field_internal_link']['id'] = 'field_internal_link';
  $handler->display->display_options['fields']['field_internal_link']['table'] = 'field_data_field_internal_link';
  $handler->display->display_options['fields']['field_internal_link']['field'] = 'field_internal_link';
  $handler->display->display_options['fields']['field_internal_link']['label'] = '';
  $handler->display->display_options['fields']['field_internal_link']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_internal_link']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['field_internal_link']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_internal_link']['empty'] = '[field_link]';
  $handler->display->display_options['fields']['field_internal_link']['hide_alter_empty'] = FALSE;
  $handler->display->display_options['fields']['field_internal_link']['type'] = 'node_reference_path';
  $handler->display->display_options['fields']['field_internal_link']['settings'] = array(
    'alias' => 1,
    'absolute' => 1,
  );
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['path'] = '[field_internal_link]';
  $handler->display->display_options['fields']['title']['alter']['alt'] = '[title]';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['element_wrapper_type'] = 'h3';
  $handler->display->display_options['fields']['title']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['alter']['max_length'] = '150';
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['body']['element_wrapper_type'] = 'p';
  $handler->display->display_options['fields']['body']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['body']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['body']['type'] = 'text_summary_or_trimmed';
  $handler->display->display_options['fields']['body']['settings'] = array(
    'trim_length' => '600',
  );
  /* Field: Content: Edit link */
  $handler->display->display_options['fields']['edit_node']['id'] = 'edit_node';
  $handler->display->display_options['fields']['edit_node']['table'] = 'views_entity_node';
  $handler->display->display_options['fields']['edit_node']['field'] = 'edit_node';
  $handler->display->display_options['fields']['edit_node']['label'] = '';
  $handler->display->display_options['fields']['edit_node']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['edit_node']['text'] = '[edit]';
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'foursitehub_resource' => 'foursitehub_resource',
  );
  /* Filter criterion: Content: Category (field_resource_category) */
  $handler->display->display_options['filters']['field_resource_category_tid']['id'] = 'field_resource_category_tid';
  $handler->display->display_options['filters']['field_resource_category_tid']['table'] = 'field_data_field_resource_category';
  $handler->display->display_options['filters']['field_resource_category_tid']['field'] = 'field_resource_category_tid';
  $handler->display->display_options['filters']['field_resource_category_tid']['value'] = array(
    51 => '51',
  );
  $handler->display->display_options['filters']['field_resource_category_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_resource_category_tid']['vocabulary'] = 'resource_category';

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Resources';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = '1';
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'foursitehub_resource' => 'foursitehub_resource',
  );
  /* Filter criterion: Content: Category (field_resource_category) */
  $handler->display->display_options['filters']['field_resource_category_tid']['id'] = 'field_resource_category_tid';
  $handler->display->display_options['filters']['field_resource_category_tid']['table'] = 'field_data_field_resource_category';
  $handler->display->display_options['filters']['field_resource_category_tid']['field'] = 'field_resource_category_tid';
  $handler->display->display_options['filters']['field_resource_category_tid']['operator'] = 'and';
  $handler->display->display_options['filters']['field_resource_category_tid']['value'] = array(
    51 => '51',
    50 => '50',
  );
  $handler->display->display_options['filters']['field_resource_category_tid']['expose']['operator_id'] = 'field_resource_category_tid_op';
  $handler->display->display_options['filters']['field_resource_category_tid']['expose']['label'] = 'Category (field_resource_category)';
  $handler->display->display_options['filters']['field_resource_category_tid']['expose']['operator'] = 'field_resource_category_tid_op';
  $handler->display->display_options['filters']['field_resource_category_tid']['expose']['identifier'] = 'field_resource_category_tid';
  $handler->display->display_options['filters']['field_resource_category_tid']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  $handler->display->display_options['filters']['field_resource_category_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_resource_category_tid']['vocabulary'] = 'resource_category';
  $handler->display->display_options['path'] = 'resources';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'Resources';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['name'] = 'main-menu';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;

  /* Display: Article */
  $handler = $view->new_display('block', 'Article', 'resource_category_article');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Article';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'foursitehub_resource' => 'foursitehub_resource',
  );
  /* Filter criterion: Content: Category (field_resource_category) */
  $handler->display->display_options['filters']['field_resource_category_tid']['id'] = 'field_resource_category_tid';
  $handler->display->display_options['filters']['field_resource_category_tid']['table'] = 'field_data_field_resource_category';
  $handler->display->display_options['filters']['field_resource_category_tid']['field'] = 'field_resource_category_tid';
  $handler->display->display_options['filters']['field_resource_category_tid']['value'] = array(
    51 => '51',
  );
  $handler->display->display_options['filters']['field_resource_category_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_resource_category_tid']['vocabulary'] = 'resource_category';

  /* Display: Video */
  $handler = $view->new_display('block', 'Video', 'resource_category_video');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Video';
  $handler->display->display_options['defaults']['css_class'] = FALSE;
  $handler->display->display_options['css_class'] = 'resource_category_video';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'foursitehub_resource' => 'foursitehub_resource',
  );
  /* Filter criterion: Content: Category (field_resource_category) */
  $handler->display->display_options['filters']['field_resource_category_tid']['id'] = 'field_resource_category_tid';
  $handler->display->display_options['filters']['field_resource_category_tid']['table'] = 'field_data_field_resource_category';
  $handler->display->display_options['filters']['field_resource_category_tid']['field'] = 'field_resource_category_tid';
  $handler->display->display_options['filters']['field_resource_category_tid']['value'] = array(
    50 => '50',
  );
  $handler->display->display_options['filters']['field_resource_category_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_resource_category_tid']['vocabulary'] = 'resource_category';

  /* Display: Blog Post */
  $handler = $view->new_display('block', 'Blog Post', 'block_1');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Blog Post';
  $handler->display->display_options['defaults']['css_class'] = FALSE;
  $handler->display->display_options['css_class'] = 'resource_category_video';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'foursitehub_resource' => 'foursitehub_resource',
  );
  /* Filter criterion: Content: Category (field_resource_category) */
  $handler->display->display_options['filters']['field_resource_category_tid']['id'] = 'field_resource_category_tid';
  $handler->display->display_options['filters']['field_resource_category_tid']['table'] = 'field_data_field_resource_category';
  $handler->display->display_options['filters']['field_resource_category_tid']['field'] = 'field_resource_category_tid';
  $handler->display->display_options['filters']['field_resource_category_tid']['value'] = array(
    52 => '52',
  );
  $handler->display->display_options['filters']['field_resource_category_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_resource_category_tid']['vocabulary'] = 'resource_category';

  /* Display: External Link */
  $handler = $view->new_display('block', 'External Link', 'block_2');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'External Link';
  $handler->display->display_options['defaults']['css_class'] = FALSE;
  $handler->display->display_options['css_class'] = 'resource_category_video';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'foursitehub_resource' => 'foursitehub_resource',
  );
  /* Filter criterion: Content: Category (field_resource_category) */
  $handler->display->display_options['filters']['field_resource_category_tid']['id'] = 'field_resource_category_tid';
  $handler->display->display_options['filters']['field_resource_category_tid']['table'] = 'field_data_field_resource_category';
  $handler->display->display_options['filters']['field_resource_category_tid']['field'] = 'field_resource_category_tid';
  $handler->display->display_options['filters']['field_resource_category_tid']['value'] = array(
    53 => '53',
  );
  $handler->display->display_options['filters']['field_resource_category_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_resource_category_tid']['vocabulary'] = 'resource_category';
  $export['resources'] = $view;

  return $export;
}
